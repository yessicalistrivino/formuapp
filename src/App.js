import './App.css';
import Header from './componentes/Header';
import UserPath from './componentes/UserPath';
import MainContainers from './componentes/MainContainer';
import UserData from './componentes/UserData';
import NavBar from './componentes/NavBar';
import React from 'react';
import DATA from './api';
import { useState } from 'react';


function App() {

  const [currentPath, setCurrentPath] = useState('sellerData');
 
  return (
    <React.Fragment>
      <Header>
      </Header>
      <UserPath 
        data={DATA}
        currentPath={currentPath}
      >
        
      </UserPath>
      <NavBar>

      </NavBar>
      <MainContainers
        data={DATA}
        currentPath={currentPath}
      >

      </MainContainers>
      <UserData>

      </UserData>
    </React.Fragment>
    
  );
}

export default App;
