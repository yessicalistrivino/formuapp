import { FaUserAlt } from 'react-icons/fa';
import { FaUserEdit } from 'react-icons/fa';
import { FaFileContract } from 'react-icons/fa';
import { FaCarAlt } from 'react-icons/fa';


const DATA = {
    
   buyerData:{
    value:"buyerData",
    uiName:"Datos del comprador",
    icon: <FaUserAlt size={30} color='#fc5130'/>,
    infoNeeded:{
        agreement:{
            uiType:"checkbox",
            uiName:"¿Eres tú el comprador?",
            value:'agreement'
        },
        names:{
            uiType:"text",
            uiName:"Nombre(s)",
            value:'names',
            placeholder:"Escribe solo tu(s) nombre(s) sin apellidos",
            successPattern:/[a-zA-Z _]{1,40}/g,
            errors:[
                { pattern:/[^a-zA-Z _()]{1,30}/g, message:'Solo se permiten letras...'}
            ],
            queryParams:{
                form:'names'
            }
        },
        surname:{
            uiType:"text",
            uiName:"Primer apellido",
            value:'surname',
            placeholder:"Escribe solo tu primer apellido",
            successPattern:/[a-zA-Z _]{1,40}/g,
            errors:[
                { pattern:/[^a-zA-Z _()]{1,30}/g, message:'Solo se permiten letras...'}
            ],
            queryParams:{
                form:'surname'
            }
        },
        secondSurname:{
            uiType:"text",
            uiName:"Segundo apellido",
            value:'secondSurname',
            placeholder:"Escribe solo tu segundo apellido",
            successPattern:/[a-zA-Z _]{1,40}/g,
            errors:[
                { pattern:/[^a-zA-Z _()]{1,30}/g, message:'Solo se permiten letras...'}
            ],
            queryParams:{
                form:'secondSurname'
            }
        },
        address:{
            uiType:"text",
            uiName:"Dirección",
            value:'address',
            placeholder:"Escribe tu dirección actual de residencia",
            successPattern:/[a-zA-Z ]{3,40}/g,
            errors:[
                { pattern:/[^a-zA-Z ]{1,30}/g, message:'Solo se permiten letras...'}
            ],
            queryParams:{
                form:'address'
            }
        },
        city:{
            uiType:"text",
            uiName:"Ciudad",
            value:'city',
            placeholder:"Escribe tu ciudad actual.",
            successPattern:/[0-9]{5,40}/g,
            errors:[
                { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
            ],
            queryParams:{
                form:'city'
            }
        },
        phone:{
            uiType:"text",
            uiName:"Teléfono",
            value:'phone',
            placeholder:"Escribe tu teléfono actual.",
            successPattern:/[0-9]{5,40}/g,
            errors:[
                { pattern:/[^0-9]/g, message:'Solo se permiten Números...'}
            ],
            queryParams:{
                form:'phone'
            }
        }

    }
   },
   sellerData:{
    value:"sellerData",
    uiName:"Datos del Vendedor",
    icon: <FaUserEdit size={35} color='#9d21a1'/>,
    infoNeeded:{
        agreement:{
            uiType:"checkbox",
            uiName:"¿Eres tú el vendedor?",
            value:'agreement'
        },
        names:{
            uiType:"text",
            uiName:"Nombre(s)",
            value:'names',
            placeholder:"Escribe solo tu(s) nombre(s) sin apellidos",
            successPattern:/[a-zA-Z _]{1,40}/g,
            errors:[
                { pattern:/[^a-zA-Z _()]{1,30}/g, message:'Solo se permiten letras...'}
            ],
            queryParams:{
                form:'names'
            }
        },
        surname:{
            uiType:"text",
            uiName:"Primer apellido",
            value:'surname',
            placeholder:"Escribe solo tu primer apellido",
            successPattern:/[a-zA-Z _]{1,40}/g,
            errors:[
                { pattern:/[^a-zA-Z _()]{1,30}/g, message:'Solo se permiten letras...'}
            ],
            queryParams:{
                form:'surname'
            }
        },
        secondSurname:{
            uiType:"text",
            uiName:"Segundo apellido",
            value:'secondSurname',
            placeholder:"Escribe solo tu segundo apellido",
            successPattern:/[a-zA-Z _]{1,40}/g,
            errors:[
                { pattern:/[^a-zA-Z _()]{1,30}/g, message:'Solo se permiten letras...'}
            ],
            queryParams:{
                form:'secondSurname'
            }
        },
        address:{
            uiType:"text",
            uiName:"Dirección",
            value:'address',
            placeholder:"Escribe tu dirección actual de residencia",
            successPattern:/[a-zA-Z ]{3,40}/g,
            errors:[
                { pattern:/[^a-zA-Z ]{1,30}/g, message:'Solo se permiten letras...'}
            ],
            queryParams:{
                form:'address'
            }
        },
        city:{
            uiType:"text",
            uiName:"Ciudad",
            value:'city',
            placeholder:"Escribe tu ciudad actual.",
            successPattern:/[0-9]{5,40}/g,
            errors:[
                { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
            ],
            queryParams:{
                form:'city'
            }
        },
        phone:{
            uiType:"text",
            uiName:"Teléfono",
            value:'phone',
            placeholder:"Escribe tu teléfono actual.",
            successPattern:/[0-9]{5,40}/g,
            errors:[
                { pattern:/[^0-9]/g, message:'Solo se permiten Números...'}
            ],
            queryParams:{
                form:'phone'
            }
        }
    },
    },
    contractData:{
        value:"contractData",
        uiName:"Datos del Contrato",
        icon: <FaFileContract size={30} color='#58a121'/>,
        infoNeeded:{
            city:{
                uiType:"text",
                uiName:"Ciudad",
                value:'city',
                placeholder:"Escribe tu ciudad actual.",
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
                ],
                queryParams:{
                    form:'city'
                }
            },
            date:{
                uiType:"date",
                uiName:"Fecha del contrato",
                value:'date',
                placeholder:"Escribe la fecha del contrato.",        
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
                ],
                queryParams:{
                    form:'date'
                }
            },
            valuevehicle:{
                uiType:"text",
                uiName:"Valor del vehículo",
                value:'valuevehicle',
                placeholder:"Escribe el valor del vehículo.",        
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
                ],
                queryParams:{
                    form:'valuevehicle'
                }
            },
            waypay:{
                uiType:"text",
                uiName:"Forma de pago.",
                value:'waypay',
                placeholder:"Escribe la forma de pago.",        
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
                ],
                queryParams:{
                    form:'waypay'
                }
            },
            valuesanction:{
                uiType:"text",
                uiName:"Valor de la sanción.",
                value:'valuesanction',
                placeholder:"Escribe el valor de la sanción.",        
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
                ],
                queryParams:{
                    form:'valuesanction'
                }
            },
            expensescharge:{
                uiType:"select",
                uiName:"Gastos a cargo de :",
                value:'expensescharge',
                placeholder:"Selecciona el responsable.",        
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
                ],
                queryParams:{
                    form:'expensescharge'
                }
            },
        },
    },
    vehicleData:{
        value:"vehicleData",
        uiName:"Datos del Vehiculo",
        icon: <FaCarAlt size={30} color='#a14d21'/>,
        infoNeeded:{
            license :{
                uiType:"text",
                uiName:"Placa (ABC123)",
                value:'license',
                placeholder:"Escribe la placa del vehículo.",
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
                ],
                queryParams:{
                    form:'license'
                }
            },
            color:{
                uiType:"text",
                uiName:"Color",
                value:'color',
                placeholder:"Escribe el color del vehículo.",        
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
                ],
                queryParams:{
                    form:'color'
                }
            },
            model:{
                uiType:"number",
                uiName:"Modelo (AAAA)",
                value:'model',
                placeholder:"Escribe el modelo del vehículo.",        
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten numeros...'}
                ],
                queryParams:{
                    form:'model'
                }
            },
            brand:{
                uiType:"text",
                uiName:"Marca.",
                value:'brand',
                placeholder:"Escribe la marca del vehículo.",        
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
                ],
                queryParams:{
                    form:'brand'
                }
            },
            line:{
                uiType:"text",
                uiName:"Línea.",
                value:'line',
                placeholder:"Escribe la Línea del vehículo.",        
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
                ],
                queryParams:{
                    form:'line'
                }
            },
            gas:{
                uiType:"text",
                uiName:"Combustible.",
                value:'gas',
                placeholder:"Escribe el tipo de combustible.",        
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
                ],
                queryParams:{
                    form:'gas'
                }
            },
            typeservice:{
                uiType:"text",
                uiName:"Tipo de Servicio.",
                value:'typeservice',
                placeholder:"Selecciona el tipo de servicio.",        
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
                ],
                queryParams:{
                    form:'typeservice'
                }
            },
            blindage:{
                uiType:"text",
                uiName:"blindaje.",
                value:'blindage',
                placeholder:"Selecciona el tipo de blindaje.",        
                successPattern:/[0-9]{5,40}/g,
                errors:[
                    { pattern:/[^0-9]/g, message:'Solo se permiten letras...'}
                ],
                queryParams:{
                    form:'blindage'
                }
            },
        }
    },
    downloadData:{
        value:"downloadData",
        uiName:"Descargar",
    },
}

export default DATA