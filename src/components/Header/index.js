import './style.css';
import { BsCheck2Square } from 'react-icons/bs';
import { FaUserCircle } from 'react-icons/fa';
import { AiOutlineCaretDown } from 'react-icons/ai';


export default function Header() {
    return (
        <section className='Header'>
            <div className='logo-header'>
                <BsCheck2Square size={60} color='white' style={{strokeWidth: '0.5px'}} />
                <div>
                    <h1 className='title'>FormuApp</h1>
                    <h4 className='slogan'>Tus Formularios Online</h4>
                </div>
            </div>
            <div className='user-login'>
                <FaUserCircle size={50} color='white' />
                <p className='name-user'>Alejandro cabrejo</p>
                <button className='button-user'><AiOutlineCaretDown size={20} color='white' /></button>
            </div>
        </section>
        
    );
}