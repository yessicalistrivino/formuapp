import './style.css';
import { AiFillCheckCircle } from 'react-icons/ai'
import { AiFillMinusCircle } from 'react-icons/ai'



export default function UserPath(props) {
    const userPath = props.data;
    const currentPath = props.currentPath;

    return (
        <div className='UserPath'>
            <h4>Formulario de Traspaso de Vehiculo</h4>
            <section className='user-path-check'>
                {
                    Object.keys(userPath).map((pathKey, index) => {
                        const Icon = currentPath === pathKey ? AiFillCheckCircle : AiFillMinusCircle;
                        const iconColor = currentPath === pathKey ? '#5cc12f' : 'white';
                
                        return (
                            <div className='' key={index}>
                                <p>{userPath[pathKey].uiName}</p>
                                <Icon size={50} color={iconColor}/>
                            </div>
                        )
                    })
                }
                
            </section>
        </div>
    );
}