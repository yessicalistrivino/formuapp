import './style.css';
import { FcIdea } from 'react-icons/fc'

export default function UserData() {
    return (
        <div className='UserData'>
            <div className='container-user-info'>
                <FcIdea size={40}/>
                <p className='info'> <span className='title-info'>Datos del Comprador: </span>Es la información personal y de
                <br />contacto de la persona que va a adquirir el bien y que
                <br />quedara con la titularidad del mismo</p>
            </div>
        </div>
    );
}