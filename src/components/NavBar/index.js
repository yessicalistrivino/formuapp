import './style.css';
import { FaHome } from 'react-icons/fa';
import { BsArrowClockwise } from 'react-icons/bs';

export default function NavBar() {
    return (
        <div className='NavBar'>
            <FaHome size={50} color='white' />
            <button className='button-nav-bar'> <BsArrowClockwise size={20} color='white'/>Reiniciar</button>
        </div>
        
    );
}