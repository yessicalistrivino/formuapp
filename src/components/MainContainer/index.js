import './style.css';
import { BsFillArrowRightCircleFill } from 'react-icons/bs';

// import UserPath from '../UserPath';

export default function MainContainers(props) {
    const userPath = props.data;
    const currentPath = props.currentPath; 
    
    return (
        <div className='MainContainer'>
            <section className={`from-container ${userPath[currentPath].value}`}>
                <div className='title-from-container'>
                    <h1 className={`h1-title-from `} >{userPath[currentPath].icon}{userPath[currentPath].uiName}</h1>                 
                </div>
                <div className={`${currentPath} from-questions`}>
                    {
                        userPath[currentPath].infoNeeded &&
                        Object.keys(userPath[currentPath].infoNeeded).map((infoKey, index) => {
                            const infoElement = userPath[currentPath].infoNeeded[infoKey];
                            console.log(infoElement);
                            return (
                                 <div className={`${currentPath} ${infoElement.value}`}>
                                    <p className=''>{(infoElement.uiName)}</p>
                                    <input className='input' type={infoElement.uiType}></input>
                                    {/* <p className={`${currentPath}`}>+ Adicionar Clausula (opcional)</p> */}
                                </div> 
                            )
                        })
                    }

                </div>
                    
                    
                
                <button className='button-from-container'>Siguiente<BsFillArrowRightCircleFill size={20}/></button>
            </section>
        </div>
    );
}


{/* <div className='agreement'>
                                        <p className=''>{(userPath[currentPath].infoNeeded.agreement.uiName)}</p>
                                        <input type={(userPath[currentPath].infoNeeded.agreement.uiType)} className='input-1'></input>
                                    </div>      */}